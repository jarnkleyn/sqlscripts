USE ModernWays;
SET sql_safe_updates=0;
UPDATE Huisdieren 
SET Leeftijd = 9
WHERE Baasje = 'Christiane' AND Soort = 'hond'
OR Baasje = 'Bert' AND Soort = 'kat'; 

SET sql_safe_updates=1;