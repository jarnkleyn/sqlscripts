use ModernWays;
SELECT  SUM(Aantalbeluisteringen), Artiest
FROM Liedjes
GROUP BY Artiest
HAVING SUM(Aantalbeluisteringen) > 100;
