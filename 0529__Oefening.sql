use Modernways;
SET sql_safe_updates= 0;

INSERT INTO Boeken (Familienaam, Titel, Verschijningsjaar,Categorie)
VALUES ('?', 'Beowulf', 1975,'Mythologie'), ('Ovidius', 'Metamorfosen', 8,'Mythologie');
SET sql_safe_updates= 1;