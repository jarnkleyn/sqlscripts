USE ModernWays;
ALTER TABLE Huisdieren add column Geluid varchar(20) char set utf8mb4;
SET sql_safe_updates= 0;
UPDATE Huisdieren
SET geluid = 'waf'
WHERE soort = 'hond';
UPDATE Huisdieren
SET geluid = 'miauw'
WHERE soort = 'kat';
SET sql_safe_updates= 1;